var alphabet = "абвгдеэжзиi,йклмнопрстуфхцчшщьюя_':.";
var keys = ["генерал", "хвиля", "олiвець", 
			"коридор", "студент", "стiлець", 
			"дерево", "вiтер", "дорога", "трамвай", 
			"радiсть", "небо", "персонал", "книжка"];


var encoded = "шчмшлтчур'г:вй'ймщибе_брхккбпр.пщксфц.лх:туемуля'иомяапьт'хьдкб_:гюдк'юсьудммтовчеял:чмiляйдухеэю'"; // ya
//var encoded = "_ам.шатллуфаойпя_мдьж_аiвл'ск:ооптрскн_х:_рнвнклльаг:жаллэанбцдфг:мэк'пячлзющм_ющдмнбч__.рьечэнхлцщ"; //gleb
//var encoded = "сцуцлйацбкиiйцюнжкбятдйждэ_вболтэйьсцуэлблн:гспвдцбжддйэбщпсiйе:ячаф'::тчдчцековдбюмхнвлб,:б"; // vlad
// var encoded = "'бргяошчюнчакйiхьиихфжщаьчещк_мцух_iьтцой_мйюжчибйяуювкдещфлєьпгнрюяиi'ьчцтгчоьбщхшьихкж:еьйбх"; // roma
// var encoded = "вум,ктрь'леег'юк_япiдюрщхат'зкдпщембваеуарн'е,ажщ:одпарьшi.яа.впбезакши'ходяхс'ььц,ммуирфюри'рiч"; // ks

function decode(text, alphabet, keys) {

	// every key
	for (var i = 0; i < keys.length; i++) {

		var key = keys[i];
		var new_key = "";
		var result = "";

		for (var z = 0; z < encoded.length; z += key.length) {
			new_key += key;
		}

		// each letter in encoded
		for (var j = 0; j < encoded.length; j++) {

			var enc_key  = alphabet.indexOf( new_key[j] );
			var enc_code = alphabet.indexOf( encoded[j] );

			if (enc_code < enc_key) {
				enc_code += alphabet.length;
			}

			var res_code = enc_code - enc_key;

			result += alphabet[res_code];
		}
		console.log("Key: %s, Result: %s", key, result);
	}
}

decode(encoded, alphabet, keys);